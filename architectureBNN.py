from torch import nn
import torch
from torchvision.transforms import *
from BBB_LRT.BBBConv import BBBConv2d


class BBB_LRT_ResMaxConvNeuralNetwork5(nn.Module):
    def __init__(self):
        super().__init__()
        dropoutProb = 1
        self.conv_stack1 = nn.Sequential(
            BBBConv2d(3, 4, kernel_size=7, stride=(2,2),padding=(3,3) ),
            nn.ReLU(),
            BBBConv2d(4, 8, kernel_size=7, stride=(2,2),padding=(3,3)),
            nn.ReLU(),
            nn.BatchNorm2d(8),)
            
        self.conv_stack2 = nn.Sequential(
            BBBConv2d(8, 12, kernel_size=7, stride=(2,2),padding=(3,3)),
            nn.ReLU(),
            BBBConv2d(12, 16, kernel_size=7, stride=(2,2),padding=(3,3)),
            nn.ReLU(),
            nn.BatchNorm2d(16),)
        self.conv_stack3 = nn.Sequential(

            BBBConv2d(16, 24, kernel_size=5, stride=(1,1)),
            nn.ReLU(),
            BBBConv2d(24, 32, kernel_size=5, stride=(1,1)),
            nn.ReLU(),
            nn.BatchNorm2d(32),)
        self.conv_stack4 = nn.Sequential(
            BBBConv2d(32, 48, kernel_size=4, stride=(1,1)),
            nn.ReLU(),
            BBBConv2d(48, 64, kernel_size=4, stride=(1,1)),
            nn.ReLU()
            ,nn.BatchNorm2d(64),)
        self.avg =  nn.Sequential(   nn.AdaptiveMaxPool2d((1,1)),
            nn.Flatten()
        )



        

        self.linear_stack = nn.Sequential(
            nn.Dropout(p=0.05),
            nn.Linear(8+16+32+64, 128),
            nn.ReLU(),
            nn.Dropout(p=0.05),
            nn.Linear(128, 512))

    def get_conv_param(self,num_layer):
        param = []
        for i in range(num_layer):
            layer = self.__getattr__(f"conv_stack{i+1}")
            for name, p in layer.named_parameters():
                if "rho" not in name:
                    param.append(p)
        return param
    def get_conv_variance(self,num_layer):
        varianceList = []
        for i in range(num_layer):
            layer = self.__getattr__(f"conv_stack{i+1}")
            for name, p in layer.named_parameters():
                if "rho" in name:
                    # variance = torch.log1p(torch.exp(p))
                    varianceList.append(p)
        return varianceList
    
    def load_param(self,model):
        for name , p in model.named_parameters():
            selfParam = self.get_parameter(name)
            if selfParam.shape == p.shape:
                selfParam.data = p.data
    def forward(self, x):
        n=4
        conv_stacks = [f"conv_stack{i+1}" for i in range(n)]
        feature = [x]
        for i in range(n):
            layer = self.__getattr__(f"conv_stack{i+1}")
            newFeature = layer(feature[-1])
            feature.append(newFeature)

        feature.pop(0)
        logits = torch.cat([self.avg(f).T for f in feature]).T
        
        logits = self.linear_stack(logits)
        
        return logits