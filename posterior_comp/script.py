import sys
import os
import logging

from fast_PCA import eigsh_func
import torch
import matplotlib.pyplot as plt
import time
from torch import autograd

from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/")
from imageNetLoader import *

from constrastiveLoss import simclr_loss_func , simclr_indiv_loss_func


myModel =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full_prune_ckpt_19")  # architecture.ResMaxConvNeuralNetwork().to(device)
parameters = myModel.get_conv_param()

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")
gradMask = []
for p in myModel.get_conv_param():
    gradMask.append(p!=0)
cpu_count = os.cpu_count()
batch_size = 256*2
num_workers= cpu_count
split_idx = [i for i in range(100+0,100+0 + 200 )]
directories =  os.listdir("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/")
directories = [d for d in directories if d[0]=="n"]
directories = directories[::1]
trainDataset = OnlineCustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",
    split_idx,transform=True,animalList = directories,contrastive=True)

train_dataloader = DataLoader(
    trainDataset, batch_size=batch_size, shuffle=True,num_workers=num_workers)
logging.info(f"cpu count = {cpu_count}")

def loss(data,model):
    X1,X2 = data
    Z1 = model(X1.to(device))
    Z2 = model(X2.to(device))
    # error= simclr_loss_func(Z1,Z2)#+ sum([p.square().sum() for p in model.parameters()])*1e-4
    error = simclr_indiv_loss_func(Z1,Z2).mean()
    # print("error=",error)
    return error

def evalHess(x):
    
    result = torch.zeros_like(x)
    epoch = 5
    for o in range(epoch):
        for i in range(x.shape[1]):
            print("eval",i)
            dir , count = [],0
            for p in parameters:
                dir.append(x[count:count+p.numel(),i].view(p.shape))
                count += p.numel()
            c = 0
            
            for batch, data in enumerate(train_dataloader):

                # Compute prediction error
                error = loss(data,myModel)
                if(batch>len(train_dataloader)):
                    break
                grad = autograd.grad(loss, parameters, create_graph=True)
                gradscal = 0
                for g, d in zip(grad, dir):
                    gradscal += (g*d).sum()

                hess = autograd.grad(gradscal, parameters)
                hess = torch.cat([h.reshape(-1) for h in hess])

                result[:,i] += hess
                c +=1
            result[:,i] = result[:,i]/c + x[:,i]*5e-3
    return result
        



d,V = eigsh_func(evalHess,torch.float32,device,D,k=5,n_iter=3)
print(d)
print(V)