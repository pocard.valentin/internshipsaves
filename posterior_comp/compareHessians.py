import matplotlib.pyplot as plt

import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/priors")
import hessian
import torch
import os
from torch import nn
from numpy.random import choice
import gaussian_filter
import architecture
import numpy as np
L2SP = False
if L2SP : hessianApprox = torch.load(f"/idiap/temp/vpocard/ImageNetTF/More_classes/ResultArch5/model_saves/L2SP/3.0_penal_hessian_run_{0}")
else : hessianApprox = torch.load(f"/idiap/temp/vpocard/ImageNetTF/More_classes/ResultArch5/model_saves/EWC/8.0_penal_hessian_run_{0}2")
penalty = torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Hessian/EWC")
# print(hessianApprox.eigen)
import math
device  ="cpu"
diag = penalty.SubHessianList[0][0].diag.to(device)
model = architecture.ResMaxConvNeuralNetwork5()
count = 0
counts = []
for i in range(4):
    layer = model.__getattr__(f"conv_stack{i+1}")

    for p in layer.parameters():
        count += p.numel()

    counts.append(count)
numEigen = 1000
W = hessianApprox.W[:numEigen].to(device)
size = 5000# W.shape[1]
index = choice(W.shape[1],size,replace=False)
index.sort()
W = W[:,index]
diag = diag[index]

eigen = hessianApprox.eigen[:numEigen].to(device)
eigen = eigen.sqrt()

W = W/W.norm(dim=1).view(-1,1)

# if not L2SP:
#     W = W/(diag.sqrt()+1e-3)

W = W*eigen[...,None]



conv = gaussian_filter.create_filter(1,5,5)
hessian = torch.mm(W.T,W)
print(hessian.shape)

# if L2SP:
#     for i in range(hessian.shape[0]):
#         # hessian[i,i] += 1e3/25000
#         hessian[i,i] = hessian[i,i]/(hessian[i,i]+1e3/25000)
# else :
#     for i in range(hessian.shape[0]):
#         # hessian[i,i] +=  diag[i]#1e3/25000
#         hessian[i,i] =   hessian[i,i]/( diag[i]+hessian[i,i]+1e-8)

print("trace = ",hessian.trace())



traces = []

for count in counts:
    indexExtract = index<count
    hessianExtract = hessian[indexExtract][:,indexExtract]
    traces.append(hessianExtract.trace().item())
traces = np.array(traces)
traces = traces/traces[-1]*100

for i in range(1,len(traces)):
    traces[-i] = traces[-i] - traces[-(i+1)]
print("traces=",traces)



# print(hessian.shape)
# print((1,)+hessian.shape)

hessian = hessian.abs() #-hessian.min()
hessian = conv(hessian.view( (1,)+hessian.shape   ))
hessian = hessian.view(hessian.shape[1:])
# hessian = hessian/hessian.max()
hessian = hessian.pow(1/5)
# 
# *255
# hessian = hessian.pow(1/5)
print(hessian.min(),hessian.max())
hessian  =hessian.detach()

hessian = hessian.cpu().numpy()

plt.imshow(hessian)#,cmap="magma"
plt.show()