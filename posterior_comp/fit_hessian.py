import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/priors")
# sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning")


import hessian
import torch
import os
from torch import nn
from torch.utils.data import DataLoader
from imageNetLoader import *
import architecture

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")
args = sys.argv[1:]
ckpt = int(args[0])
run = ckpt
logging.basicConfig(filename=f"/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/Grid_Output/logger_{args[0]}",
                    filemode='a')
directories =  os.listdir("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/")
directories = [d for d in directories if d[0]=="n"]
labels = np.load("/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/labels2.npy")

directories = [directories[index] for index in labels[run]]

split_idx = [i for i in range(100,100+50 )]
trainDataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",split_idx,transform=False,animalList = directories)

cpu_count = os.cpu_count()
batch_size = 1
num_workers= cpu_count
train_dataloader = DataLoader(
    trainDataset, batch_size=batch_size, shuffle=True,num_workers=num_workers)

myModel =  torch.load(f"/idiap/temp/vpocard/ImageNetTF/More_classes/ResultArch5/model_saves/L2SP/3.0_penal_model_run_{run}") 
myModel =  torch.load(f"/idiap/temp/vpocard/ImageNetTF/More_classes/ResultArch5/model_saves/EWC/8.0_penal_model_run_{run}") 
penalite = torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Hessian/EWC")

loss_fn = nn.CrossEntropyLoss()
startingPoint = torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full2_ckpt_19")
startingPoint.linear_stack = architecture.ResMaxConvNeuralNetwork5().to(device).linear_stack
startingConv = [p.detach() for p in startingPoint.get_conv_param() ]
myModel.eval()
def L2SPprior(model):
    pen = sum([ (sP-p).square().sum() for (sP,p) in zip(startingConv,model.get_conv_param())  ])
    return pen*1e3/len(trainDataset)
def HESSprior(model):
    pen = penalite.error(model)*1e8/len(trainDataset)
    return pen
prior = HESSprior
diag = penalite.SubHessianList[0][0].diag.to(device)
gradMask = 1/(diag.sqrt()+1e-3)


def loss(data,model):
    X,y = data
    pred = model(X.to(device))
    y = y.to(device)
    error = loss_fn(pred,y)
    return error

def test(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    model.eval()
    test_loss, correct , correct5 = 0, 0,0
    with torch.no_grad():
        for X, y in tqdm( dataloader):
            X, y = X.to(device), y.to(device)
            pred = model(X)
            test_loss += loss_fn(pred, y).item()
            correct += (pred.argmax(1) == y).type(torch.float).sum().item()
            top5 = torch.topk(pred,5,dim=1).indices
            correct5 += (top5.T==y).type(torch.float).sum().item()

            
    test_loss /= num_batches
    correct /= size
    correct5 /= size
    print(
        f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Accuracy5: {(100*correct5):>0.1f}%  ,  Avg loss: {test_loss:>8f} \n")
    return correct, test_loss , correct5

# test(train_dataloader,myModel,loss_fn)
# test(train_dataloader,startingPoint,loss_fn)


## fit hessian
for p in myModel.get_conv_param():
    p.requires_grad = True
for p in myModel.linear_stack.parameters():
    p.requires_grad = False

D = sum([p.numel() for p in myModel.parameters() if p.requires_grad])

hessianApprox = hessian.HessianApproxFastPCA(D,device,None,rank=1000,buffer_size= 1000)
for i in range(1):
    hessian.fit_hessianApprox_general2(myModel,train_dataloader,loss,prior,hessianApprox,num_samples=len(trainDataset),gradMask=gradMask)
    print(hessianApprox.diag.min())

    # torch.save(hessianApprox,f"/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/ResultArch5/model_saves/L2SP/3.0_penal_hessian_run_{run}")
    torch.save(hessianApprox,f"/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/ResultArch5/model_saves/EWC/8.0_penal_hessian_run_{run}2")
