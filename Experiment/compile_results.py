import numpy as np
import
resultHess = []


for conv in [1,2,3,4]:
    path = f"/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/ResultHessian/Conv{conv}/result.npy"
    result = np.load(path)
    indice = np.argmax(result[:,0])
    print(f"result conv{conv} = {result[indice]}")
    resultHess.append(result[indice])

resultId = np.load("/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/ResultId/result.npy")
print(resultId)

for i in range(3):
    plt.subplo