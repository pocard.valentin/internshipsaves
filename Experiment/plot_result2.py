import os
import numpy as np
import matplotlib.pyplot as plt


path = "/idiap/temp/vpocard/ImageNetTF/More_classes/Result_multiple_runs/"
files = os.listdir(path)
X,datas = [],[]

f = f"150_samples_ckpt"
# data = np.load("/idiap/temp/vpocard/ImageNetTF/More_classes/Result_SS/EWC_3/7.0_penal_50_samples_run0.npy")
# data = np.load("/idiap/temp/vpocard/ImageNetTF/More_classes/Train_Expert/Result/0.1/1.0_penal_50_samples_run1.npy")
# data = np.load("/idiap/temp/vpocard/ImageNetTF/More_classes/Train_Expert/Result_differentSplit/29/3.0_penal_150_samples_run0.npy")
data = np.load("/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/ResultId/Conv3/3.0_penal_50_samples_run2.npy")

# data = np.load("/idiap/temp/vpocard/ImageNetTF/More_classes/Train_Expert/Result_prune_EWC_temp/1e10/3.3_penal_300_samples_run15.npy")

# dataF = np.zeros_like(data)
# alpha = 0.5
# dataF[0] = data[0]
# for i in range(1,data.shape[0]):
#     dataF[i] = dataF[i-1]*(alpha) + (1-alpha)*data[i]
# data = dataF

# data = data[:100,:]
print(data.shape)
datas.append([data[-1,0]*100,data[-1,2]*100,data[-1,1]])

# X.append( int(f.split("_")[0]) )

# print(data)
print(data[-1,0]*100,data[-1,2]*100,data[-1,1])
print(max(data[:,0]*100),max(data[:,2]*100),min(data[:,1]))
plt.subplot(3,1,1)
plt.title(f)
plt.plot(data[:,0]*100)

plt.subplot(3,1,2)
plt.plot(data[:,1])

plt.subplot(3,1,3)
plt.plot(data[:,2]*100)
plt.show()
plt.savefig("/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/Graphics/plot_result2")



