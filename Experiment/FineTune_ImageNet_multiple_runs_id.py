import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/priors/")

import numpy as np

import torch

from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
import os
import architecture
from imageNetLoader import *
import SWAG
# from evidence import compute_LogEvidence


# directories = np.load("/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/Saves/RepresentationList.npy")

args = sys.argv[1:]
logPen = float(args[0])
offset = int( args[1])
conv = int(args[2])


# split_idx = [i for i in range(100)]
# testADataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",split_idx,transform=False,animalList = directories)


device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")
myModel =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full2_ckpt_19")  # architecture.ResMaxConvNeuralNetwork().to(device)
myModel =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full_prune_ckpt_19")  # architecture.ResMaxConvNeuralNetwork().to(device)
gradMask = []
for p in myModel.get_conv_param():
    gradMask.append(p!=0)
myModel.linear_stack = architecture.ResMaxConvNeuralNetwork5().to(device).linear_stack


startingPoint = [p.clone().detach() for p in myModel.parameters()]
startingConv = [p.clone().detach() for p in myModel.get_conv_param()]
# startingConv = [torch.zeros_like(p)for p in myModel.get_conv_param()]
startingLinear = [torch.zeros_like(p) for p in myModel.linear_stack.parameters()]
print(sum([p.norm() for p in startingLinear]))
# swag = torch.load("/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/Saves/swag")
# startingPoint = SWAG.castVec2Param(myModel,swag.mean)

cpu_count =  os.cpu_count()
logging.info(f"cpu count = {cpu_count}")
D = sum([p.numel() for p in myModel.parameters()])
print(f"Number of parameters : {D}")


logging.info(f"regularization strength = 10**{logPen}")



# testA_dataloader = DataLoader(
#     testADataset, batch_size=batch_size*4, shuffle=False,num_workers=num_workers,pin_memory=True)

def computeNorm(param1,param2):
    sqNorm = 0
    for p1,p2 in zip(param1,param2):
        sqNorm += (p1-p2).square().sum()
    return sqNorm
def penalty(model,lambdaPen):
    convPenal = computeNorm(model.get_conv_param(),startingConv)*lambdaPen
    linPenal  = computeNorm(model.linear_stack.parameters(),startingLinear)*1.5e1
    return convPenal+linPenal


def train(dataloader, model, loss_fn, optimizer):
    
    model.train()
    for batch, (X, y) in enumerate(dataloader):
        X, y = X.to(device), y.to(device)
        pred = model(X)
        likehood = loss_fn(pred, y)
        logPrior = penalty(model,10**logPen)/size
        loss = likehood + logPrior
        
        optimizer.zero_grad()
        loss.backward()
        for p,m in zip(model.get_conv_param(),gradMask):
            if p.requires_grad:
                p.grad = p.grad*m
        optimizer.step()
        if batch % 50 == 0:
            loss, current = likehood.item(), batch * len(X)
            print(
                f"loss: {loss:>7f} logPrior : {logPrior.item():>7f} [{current:>5d}/{size:>5d}]")


def test(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    model.eval()
    test_loss, correct , correct5 = 0, 0,0
    with torch.no_grad():
        for X, y in dataloader:
            X, y = X.to(device), y.to(device)
            pred = model(X)
            test_loss += loss_fn(pred, y).item()
            correct += (pred.argmax(1) == y).type(torch.float).sum().item()
            top5 = torch.topk(pred,5,dim=1).indices
            correct5 += (top5.T==y).type(torch.float).sum().item()

            
    test_loss /= num_batches
    correct /= size
    correct5 /= size
    logging.info(
        f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Accuracy5: {(100*correct5):>0.1f}%  ,  Avg loss: {test_loss:>8f} \n")
    return correct, test_loss , correct5

epochs = 100
loss_fn = nn.CrossEntropyLoss()

# param = myModel.get_conv_param()
# for p in param:
#     p.requires_grad = False

D = sum([p.numel() for p in myModel.parameters() if p.requires_grad])
print(f"Number of trainable parameters : {D}")
nb_runs = 1
subPath = f"Conv{conv}"
evidence = [ ]
labels = np.load("/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/labels2.npy")
# modelA =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_2_ckpt_65")
offset = int(offset)
for run in range(offset,offset+ nb_runs):

    directories =  os.listdir("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/")
    directories = [d for d in directories if d[0]=="n"]
    
    directories = [directories[index] for index in labels[run]]
    split_idx = [i for i in range(50)]
    path = f"/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/ResultIdSGD/{subPath}/{logPen}_penal_{len(split_idx)}_samples_run{run}"
    skip = False
    try:
        data = np.load(path+".npy")
        skip = ( data.shape[0] == epochs)
    except:
        print("error reading file")
        pass
    # if skip:
    #     print("skipped")
    #     continue

    testDataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",split_idx,transform=False,animalList = directories)
    split_idx = [i for i in range(100,100+50 )]
    trainDataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",split_idx,transform=False,animalList = directories)
    batch_size = 64
    num_workers=cpu_count
    train_dataloader = DataLoader(
        trainDataset, batch_size=batch_size, shuffle=True,num_workers=num_workers)
    test_dataloader = DataLoader(
        testDataset, batch_size=batch_size*4, shuffle=False,num_workers=num_workers)
    size = len(train_dataloader.dataset)

    myModel =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full_prune_ckpt_19")
    myModel.linear_stack = architecture.ResMaxConvNeuralNetwork5().to(device).linear_stack

    parameters = []
    for p in myModel.parameters():
        p.requires_grad = False

    for convNumber in range(1,1+conv):
        layer = myModel.__getattr__(f"conv_stack{convNumber}")
        for p in layer.parameters():
            parameters.append(p)
            p.requires_grad = True
    
    for p in myModel.linear_stack.parameters():
        parameters.append(p)
        p.requires_grad = True

    optimizer = torch.optim.SGD(parameters,lr=1e-3,momentum=0.99)#  torch.optim.Adam(parameters, lr=5e-4)
    # optimizer = torch.optim.Adam(myModel.linear_stack.parameters(), lr=1e-3)

    # scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min',patience=3,factor=0.5)

    scheduler = torch.optim.lr_scheduler.StepLR(optimizer,2,0.90)
    datas = []
    for t in range(epochs):
        print(f"Epoch {t+1}\n-------------------------------")

        if t%2==0 : data= list(test(test_dataloader, myModel, loss_fn))
        # for pA,p in zip(modelA.get_conv_param(),myModel.get_conv_param()):
        #     pA=p

        # data2 = test(testA_dataloader, myModel, loss_fn)
        # # scheduler.step(data[1])#test_loss
        # for d in data2:
        #     data.append(d)
        print('Epoch-{0} lr: {1}'.format(t+1, optimizer.param_groups[0]['lr']))

        train(train_dataloader, myModel, loss_fn, optimizer)

        # train_dataloader.num_workers =cpu_count #Allow to cache data during the first epoch
        # test_dataloader.num_workers = cpu_count #Allow to cache data during the first epoch
        temp = 0
        for p,m in zip(myModel.get_conv_param(),gradMask):
            if p.requires_grad:
                temp += p[ m==False].square().sum()
        print("temp=",temp)
        scheduler.step()
        datas.append(data)
        np.save(path,datas)
    del trainDataset ,  testDataset , train_dataloader,test_dataloader 
    torch.save(myModel,f"/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/ResultIdSGD/{subPath}/{logPen}_penal_model_run_{run}")
    # logEv = compute_LogEvidence(train_dataloader,myModel,loss_fn,lambda x : penalty(x,10**logPen/size),10**logPen/size)
    
    # evidence.append(logEv.cpu().numpy())
    # np.save(f"/idiap/temp/vpocard/ImageNetTF/More_classes/Result_scalar_pen/{path}/evidence_{logPen}_penal_{len(split_idx)}_samples",evidence)
