import numpy as np
import matplotlib
# matplotlib.use('PDF')
import matplotlib.pyplot as plt
import os
import regex
import pandas as pd
folders = ["L2SP","HESS","EWC"]

# folders = ["L2SP_2","HESS_2","EWC_2"]
# folders = ["L2SP_3","HESS_3","EWC_3"]
# n=2
# folders = [f"L2SP_{n}",f"HESS_{n}",f"EWC_{n}"]
mainPath = "/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/ResultId/"
# mainPath = "/idiap/temp/vpocard/ImageNetTF/More_classes/ResultArch5/Hess_id/"
# folders = ["0.0Hess_id","2.0Hess_id","3.0Hess_id","4.0Hess_id","6.0Hess_id","../baselines/L2SP"]
folders = [f"Conv{i}" for i in range(1,5)]

pdResult = pd.DataFrame(index=folders)
print(pdResult)

# folders = ["0.1","0.01","0.001","0.0001"]#,"0.0"]
# folders = [f"1e-{i}" for i in range(0,8,2)]
# folders[0] = "1e0"

legend = {}
for f in folders:
    legend[f] = str(f)
    # if "prune" in mainPath:
    #     legend[f] = str(f)
    #     continue

    # if "e" in f:
    #     legend[f] = f"{100*float(f)}%"
    # else:
    #     legend[f] = f"{100-float(f)*100}%"


titles = ["TOP 1 ACCURACY", "TEST LOSS","TOP 5 ACCURACY"]
lines = [1,2,3,2]
ylabels = ["accuracy (%)" , "loss","accuracy (%)" ]

text = ""
result = []
test = []
for f in folders:
    path = mainPath + f + "/"
    X = []
    datas = {}
    
    for file in os.listdir(path):
        if not "npy" in file:
            continue
        if "evidence" in file:
            continue
        

        logPen = float( file.split("_")[0])

        run = int( file.split("run")[-1].split(".")[0])
      
        # if run > 8:
        #     continue
        # if regex.search("run[1-2][0-9]" ,file):
        #     # print(file," skipped")
        #     continue


        
        # if file[:3] != "3.0":
        #     continue

        

        data = np.load(path+file)*100
        if data.shape[0] < 100:
            text = (path+file).split("/")[-2:]
            print(text)
            print(data.shape)

        if data.shape[0] < 75:
            continue
        if data.shape[1] == 4:
            data = data[:,:3]
        data = data[:100,:3]
        if not logPen in X:
            X.append(logPen)
            datas[logPen] = []
            if logPen not in pdResult:
                pdResult[logPen] = 0
        pdResult[logPen][f] += 1
        data=data[:100,:]
        data[:,1] = data[:,1]/100
        for i in [1  ]:# # range(1,3):
            datas[logPen].append(data[-i].tolist())#.mean(axis=0)


    X.sort()
    if X == []:
        continue
    while X[0] < -3:
        X.pop(0)

    means , stds = [] , []
    for x in X:
        
        npArray = np.array(datas[x])
        print(x,npArray.shape[0])
        # print(npArray.shape)
        means.append(npArray.mean(axis=0).tolist())
        # means.append( np.median(npArray,0).tolist())
        stds.append(npArray.std(axis=0).tolist()/np.sqrt(npArray.shape[0]) )
        if np.isnan( means[-1]).any():
            means[-1] = [0 ,0 ,0]
            stds[-1]  = [0, 0 ,0]
    means = np.array(means)
    stds = np.array(stds)
    # print(means)
    for i in range(3):
        
        plt.subplot(3,1,lines[i])
        plt.title(titles[i])
        plt.errorbar(X,means[:,i],stds[:,i],label=legend[f])
        plt.ylabel(ylabels[i])
        
        plt.xlabel("Regularization strength (log scale)")
        plt.legend()
    text += (f"max {f} =  {means.max(axis=0)[0],means.max(axis=0)[2],means.min(axis=0)[1]}\n")

    indexSTD = [means.argmax(axis=0)[0] , means.argmin(axis=0)[0] ,means.argmax(axis=0)[0]  ]
    var = [ means.max(axis=0)[0],means.min(axis=0)[1],means.max(axis=0)[2] ]
    for i in range(3):
        var.append(stds[indexSTD[i],i])
    conv = int(f[-1])
    var.append((conv))
    var.append(X[indexSTD[0]])
    result.append(var  )
pdResult = pdResult.reindex(sorted(pdResult.columns), axis=1)
print(pdResult)
plt.legend()
print("show pic")
plt.show()
print("pic showed")
plt.savefig("/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/Graphics/lossPlot3")
result = np.array(result)
print(result)
np.save(mainPath+"result",result)
for i in range(3):
        
        plt.subplot(3,1,lines[i])
        plt.title(titles[i])
        # plt.errorbar(X,means[:,i],stds[:,i],label=f)
        # data = [ (  119244- r[-1]*5000,r[i],r[i+3]) for r in result]
        data = [ (  r[-2],r[i],r[i+3]) for r in result]
        print(data)
        data.sort()
        plt.errorbar(range(len(data)),[d[1] for d in data],[d[2] for d in data])
        # plt.plot(range(len(result)),[r for r in reversed( result[:,i])])
        xLabel = [d[0] for d in data]
        # xLabel.reverse()
        plt.xticks(range(len(result)),xLabel)
        plt.ylabel(ylabels[i])
        plt.xlabel("Precision trained")
        # plt.legend()
plt.show()
