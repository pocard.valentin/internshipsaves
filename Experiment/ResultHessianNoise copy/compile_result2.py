import numpy as np
import matplotlib
# matplotlib.use('PDF')
import matplotlib.pyplot as plt
import os
import regex
import pandas as pd

conv = 4
mainPath = f"/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/ResultHessianNoise/Conv{conv}/"
folders = ["-1.0","0.0","1.0","2.0","3.0"]



pdCount = pd.DataFrame(index=folders)
print(pdCount)

meansDict = {}
stdsDict  = {}

legend = {}
for f in folders:
    legend[f] = str(f)


titles = ["TOP 1 ACCURACY", "TEST LOSS","TOP 5 ACCURACY"]
lines = [1,2,3,2]
ylabels = ["accuracy (%)" , "loss","accuracy (%)" ]

text = ""
result = []
test = []
for f in folders:

    path = mainPath + f + "/"
    print(path)
    X = []
    datas = {}
    
    for file in os.listdir(path):
        if not "npy" in file:
            continue
        if "evidence" in file:
            continue
        

        logPen = float( file.split("_")[0])

        run = int( file.split("run")[-1].split(".")[0])
        if run >2:
            continue
        

        data = np.load(path+file)*100
        if data.shape[0] < 100:
            text = (path+file).split("/")[-2:]
            print(text)
            print(data.shape)

        if data.shape[0] < 75:
            continue
        if data.shape[1] == 4:
            data = data[:,:3]
        data = data[:100,:3]
        if not logPen in X:
            X.append(logPen)
            datas[logPen] = []
            if logPen not in pdCount:
                pdCount[logPen] = 0
        pdCount[logPen][f] += 1
        data=data[:100,:]
        data[:,1] = data[:,1]/100
        for i in [1  ]:# # range(1,3):
            datas[logPen].append(data[-i].tolist())#.mean(axis=0)


    X.sort()
    if X == []:
        continue
    while X[0] < -3:
        X.pop(0)

    means , stds = [] , []
    meansDict[f] = {}
    stdsDict[f] = {}
    for x in X:
        
        npArray = np.array(datas[x])
        print(x,npArray.shape[0])
        # print(npArray.shape)
        means.append(npArray.mean(axis=0).tolist())
        # means.append( np.median(npArray,0).tolist())
        stds.append(npArray.std(axis=0).tolist()/np.sqrt(npArray.shape[0]) )

        

        if np.isnan( means[-1]).any():
            means[-1] = [0 ,0 ,0]
            stds[-1]  = [0, 0 ,0]
        
        meansDict[f][x] = means[-1]
        stdsDict[f][x]  = stds[-1]
        
    means = np.array(means)
    stds = np.array(stds)
    # print(means)
    for i in range(3):
        
        plt.subplot(3,1,lines[i])
        plt.title(titles[i])
        plt.errorbar(X,means[:,i],stds[:,i],label=legend[f])
        plt.ylabel(ylabels[i])
        
        plt.xlabel("Regularization strength (log scale)")
        plt.legend()
    text += (f"max {f} =  {means.max(axis=0)[0],means.max(axis=0)[2],means.min(axis=0)[1]}\n")

    indexSTD = [means.argmax(axis=0)[0] , means.argmin(axis=0)[0] ,means.argmax(axis=0)[0]  ]
    var = [ means.max(axis=0)[0],means.min(axis=0)[1],means.max(axis=0)[2] ]
    for i in range(3):
        var.append(stds[indexSTD[i],i])
    var.append((f))
    var.append(X[indexSTD[0]])
    result.append(var  )
pdCount = pdCount.reindex(sorted(pdCount.columns), axis=1)
print(pdCount)
print(pdCount.sum().sum())
plt.legend()
print("show pic")
plt.show()
print("pic showed")
plt.savefig("/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/Graphics/lossPlot1")
plt.close()
result = np.array(result,dtype=np.float32)
print(result)

np.save(mainPath+"result",result)
for i in range(3):
    Y = [ r[i] for r in result  ]
    stdY = [ r[i+3] for r in result  ]
    X = [  r[-2] for r in result ]
    plt.subplot(3,1,i+1)
    plt.errorbar(X,Y,stdY)
plt.show()
plt.savefig("/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/Graphics/lossPlot2")
