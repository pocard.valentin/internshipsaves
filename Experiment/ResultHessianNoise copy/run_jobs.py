import numpy as np
import os
conv = [i for i in range(1,5)]
pen2 = [0 , 1 , 2]
pen = [2,3,4]
offset = [0,1,2]
count = 0
for convElem in conv:
    for pen2Elem in pen2:
        for penElem in pen:
            for offsetElem in offset:
                path = f"/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment/ResultHessianNoise/Conv{convElem}/{pen2Elem}.0/{penElem}.0_penal_{50}_samples_run{offsetElem}"
                # print(path)
                skip = False
                try:
                    data = np.load(path+".npy")
                    skip =  ( data.shape[0] == 100)
                    # print("skip=",skip)
                except:
                    pass
                # print(skip)
                if skip: continue
                commande = f"qsub job6 {penElem} {pen2Elem} {offsetElem} {convElem} "
                print(commande)
                # input()
                os.system(commande)
                count += 1

print("total jobs sent =",count)