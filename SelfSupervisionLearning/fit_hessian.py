import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/priors")
# sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning")


import hessian
import torch
import os
from torch import nn
from torch.utils.data import DataLoader
from imageNetLoader import *
from constrastiveLoss import simclr_loss_func , simclr_indiv_loss_func

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")
args = sys.argv[1:]
ckpt = int(args[0])
logging.basicConfig(filename=f"/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/Grid_Output/logger_{args[0]}",
                    filemode='a')
directories =  os.listdir("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/")
directories = [d for d in directories if d[0]=="n"]
directories = directories[::1]

# split_idx = [i for i in range(10)]
# testDataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",
#     split_idx,transform=True,animalList = directories,contrastive=True)

split_idx = [i for i in range(100+0,100+0 + 100 )]
trainDataset = OnlineCustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",
    split_idx,transform=True,animalList = directories,contrastive=True)

cpu_count = os.cpu_count()
batch_size = 256*2
num_workers= cpu_count
train_dataloader = DataLoader(
    trainDataset, batch_size=batch_size, shuffle=True,num_workers=num_workers)

myModel =  torch.load(f"/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full2_ckpt_19") 

def loss(data,model):
    X1,X2 = data
    Z1 = model(X1.to(device))
    Z2 = model(X2.to(device))
    # error= simclr_loss_func(Z1,Z2)#+ sum([p.square().sum() for p in model.parameters()])*1e-4
    error = simclr_indiv_loss_func(Z1,Z2)
    # print("error=",error)
    return error


# #WARMUP
# optimizer = torch.optim.Adam(myModel.linear_stack.parameters(),lr=5e-4)
# scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.5)

# for p in myModel.get_conv_param():
#     p.requires_grad = False

# for epoch in range(10):
#     for batch , data in enumerate(train_dataloader):
#         optimizer.zero_grad()
#         error = loss(data,myModel)
#         error.backward()
#         optimizer.step()
#         if (batch+1) %10==0:
#             print("train loss = ",error.item(), f" progress = {batch+1}/{len(train_dataloader)}")
#     scheduler.step()

## fit hessian
for p in myModel.get_conv_param():
    p.requires_grad = True
for p in myModel.linear_stack.parameters():
    p.requires_grad = False

D = sum([p.numel() for p in myModel.parameters() if p.requires_grad])
gradMask = torch.cat([(p != 0).view(-1) for p in myModel.get_conv_param()])
gradMask = torch.ones_like(gradMask)
# gradMask = torch.cat([gradMask , torch.ones(sum([p.numel() for p in myModel.linear_stack.parameters()]),device=device)])

hessianApprox = hessian.HessianApproxFastPCA(D,device,None,rank=20,buffer_size= 75)
for i in range(5):
    hessian.fit_hessianApprox_general3(myModel,train_dataloader,loss,lambda model: 0,hessianApprox,num_samples=150,gradMask=gradMask)
    print(hessianApprox.diag[gradMask].min())
    # hessianApprox.buffer = None
    torch.save(hessianApprox,f"/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/SS-learning/Saves/hessian2_model_full")