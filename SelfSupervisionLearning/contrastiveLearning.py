import torch
# import matplotlib.pyplot as plt
import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")

from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
import numpy as np
import os
import torch.nn.functional as F
import training_func 
import architecture
from imageNetLoader import *
import copy
from constrastiveLoss import simclr_loss_func

logging.getLogger().setLevel(20)
device = "cuda" if torch.cuda.is_available() else "cpu"
logging.info(f"Using {device} device")
model =torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full2_ckpt_19")# architecture.ResMaxConvNeuralNetwork6().to(device)
# model =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_prune_ckpt_44")
# model.linear_stack = nn.Sequential(
#             nn.Dropout(p=0.05),
#             nn.Linear(8+16, 128),
#             nn.ReLU(),
#             nn.Dropout(p=0.05),
#             nn.Linear(128, 128)).cuda()
# for p in model.conv_stack4.parameters():
#     p.data = torch.zeros_like(p)
#     p.requires_grad = False

directories =  os.listdir("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/")
directories = [d for d in directories if d[0]=="n"]
directories = directories[::1]

split_idx = [i for i in range(20)]
testDataset = OnlineCustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",
    split_idx,transform=True,animalList = directories,contrastive=True)

split_idx = [i for i in range(100+0,100+0 + 200 )]
trainDataset = OnlineCustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",
    split_idx,transform=True,animalList = directories,contrastive=True)

cpu_count = os.cpu_count()
batch_size = 512
num_workers= cpu_count
train_dataloader = DataLoader(
    trainDataset, batch_size=batch_size, shuffle=True,num_workers=num_workers)
test_dataloader = DataLoader(
    testDataset, batch_size=batch_size, shuffle=False,num_workers=num_workers)



train_step = int(5e2)
shape =  trainDataset.__getitem__(0)[0].shape #( trainDataset.__getitem__(0)[0].shape[1],trainDataset.__getitem__(0)[0].shape[2])
optimizer = torch.optim.Adam(model.parameters(),lr= 1e-3,betas=[0.9,0.95])
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.999)
gradMean = None
def train(dataloader,model):
    model.train()
    for batch , (X1,X2) in enumerate( dataloader):
        Z1 = model(X1.to(device))
        Z2 = model(X2.to(device))

        loss = simclr_loss_func(Z1,Z2)# + sum([p.square().sum() for p in model.parameters()])*1e-4
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        

        if (batch+1) %5==0:
            
            momentumNorm = 0
            squareGrad = 0
            for p in model.parameters():
                momentumNorm += optimizer.state[p]['exp_avg'].square().sum()
                squareGrad   += optimizer.state[p]['exp_avg_sq'].sum()

            logging.info(f"train loss = {loss.item()} progress = {batch+1}/{len(dataloader)}")
            logging.info(f"mean ={momentumNorm }  square Mean = {squareGrad}")
        scheduler.step()

def test(dataloader,model):
    model.eval()
    totalLoss = 0
    with torch.no_grad():
        for batch , (X1,X2) in enumerate(dataloader):
            Z1 = model(X1.to(device))
            Z2 = model(X2.to(device))

            totalLoss += simclr_loss_func(Z1,Z2)/len(dataloader)
            

    logging.info(f"Test loss ={totalLoss.item()}" )

for t in range(train_step):
    logging.info("TEST")
    test(test_dataloader,model)
    logging.info(f"TRAIN epoch {t+1}")
    train(train_dataloader,model)
    scheduler.step()
    
    
    # logging.info("COPY")
    # modelSave = copy.deepcopy(model)
    # modelSave.linear_stack = architecture.ResMaxConvNeuralNetwork4().linear_stack.to(device)
    logging.info("SAVE")
    if (t+1)%5==0:
        torch.save(model,f"/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full2_prune_ckpt_{t}")
    # del modelSave
    

    