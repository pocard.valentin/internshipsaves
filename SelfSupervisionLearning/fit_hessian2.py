import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/priors")
# sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning")


import hessian
import torch
import os
from torch import nn
from torch.utils.data import DataLoader
from imageNetLoader import *
from constrastiveLoss import simclr_loss_func , simclr_indiv_loss_func

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

directories =  os.listdir("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/")
directories = [d for d in directories if d[0]=="n"]
directories = directories[::1][:256]

split_idx = [i for i in range(10)]

split_idx = [i for i in range(100+0,100+0 + 150 )]
trainDataset = OnlineCustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",
    split_idx,transform=True,animalList = directories,contrastive=True)

cpu_count = os.cpu_count()
batch_size = 256*2
num_workers= cpu_count
train_dataloader = DataLoader(
    trainDataset, batch_size=batch_size, shuffle=True,num_workers=num_workers)

myModel =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_2_ckpt_65") 
for p in myModel.get_conv_param():
    p.requires_grad = True
for p in myModel.linear_stack.parameters():
    p.requires_grad = False

D = sum([p.numel() for p in myModel.parameters() if p.requires_grad])


hessianApprox = hessian.HessianApproxFastPCA(D,device,None,rank=20,buffer_size= 200)# len(train_dataloader))
num_samples = 256
for epoch in range(5):
    for  (X1,X2) in tqdm (train_dataloader):
        Z1 = myModel(X1.to(device))
        Z2 = myModel(X2.to(device))
        error = simclr_indiv_loss_func(Z1,Z2)

        grad_mean = None
        grad_norm_mean = 0
        print("error=",sum(error))
        for iter ,  e  in enumerate(tqdm( error)):
            if iter == num_samples:
                break
            for p in myModel.get_conv_param():
                p.grad = torch.zeros_like(p)

            e.backward(retain_graph= (iter+1 < num_samples ))
            grad = torch.cat([p.grad.view(-1).detach() for p in myModel.parameters() if p.requires_grad])
            hessianApprox.collect_grad(grad)

            if grad_mean == None:
                grad_mean = grad
            else:
                grad_mean += grad/(num_samples)
            
            grad_norm_mean +=  grad.square().sum()/(num_samples)


        print(grad_mean.norm(),grad_norm_mean.sqrt())