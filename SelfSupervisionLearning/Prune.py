import torch
# import matplotlib.pyplot as plt
import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")

from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
import numpy as np
import os
import torch.nn.functional as F
import training_func 
import architecture
from imageNetLoader import *
import copy
from constrastiveLoss import simclr_loss_func

logging.getLogger().setLevel(20)
device = "cuda" if torch.cuda.is_available() else "cpu"
logging.info(f"Using {device} device")
# model = architecture.ResMaxConvNeuralNetwork4().to(device)
model =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full2_ckpt_19")


directories =  os.listdir("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/")
directories = [d for d in directories if d[0]=="n"]
directories = directories[::1]

split_idx = [i for i in range(20)]
testDataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",
    split_idx,transform=True,animalList = directories,contrastive=True)

split_idx = [i for i in range(100+0,100+0 + 200 )]
trainDataset = OnlineCustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",
    split_idx,transform=True,animalList = directories,contrastive=True)

cpu_count = os.cpu_count()
batch_size = 256*2
num_workers= cpu_count
train_dataloader = DataLoader(
    trainDataset, batch_size=batch_size, shuffle=True,num_workers=num_workers)
test_dataloader = DataLoader(
    testDataset, batch_size=batch_size, shuffle=False,num_workers=num_workers)

gradMask = []
for p in model.get_conv_param():
    gradMask.append(torch.ones_like(p))

train_step = int(5e2)
shape =  trainDataset.__getitem__(0)[0].shape #( trainDataset.__getitem__(0)[0].shape[1],trainDataset.__getitem__(0)[0].shape[2])
optimizer = torch.optim.Adam(model.parameters(),lr= 1e-4,betas=[0.9,0.99])
# scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.90)
gradMean = None
testLoss = []
def train(dataloader,model):
    model.train()
    for batch , (X1,X2) in enumerate( dataloader):
        Z1 = model(X1.to(device))
        Z2 = model(X2.to(device))

        loss = simclr_loss_func(Z1,Z2) + sum([p.square().sum() for p in model.parameters()])*1e-4
        optimizer.zero_grad()
        loss.backward()
        for p,mask in zip(model.get_conv_param(),gradMask):
            p.grad = p.grad*mask
        
        optimizer.step()
        

        if (batch+1) %10==0:
            
            momentumNorm = 0
            squareGrad = 0
            for p in model.parameters():
                momentumNorm += optimizer.state[p]['exp_avg'].square().sum()
                squareGrad   += optimizer.state[p]['exp_avg_sq'].sum()

            print("train loss = ",loss.item(), f" progress = {batch+1}/{len(dataloader)}")
            print(f"mean ={momentumNorm }  square Mean = {squareGrad}")

def test(dataloader,model):
    model.eval()
    totalLoss = 0
    with torch.no_grad():
        for batch , (X1,X2) in enumerate(dataloader):
            Z1 = model(X1.to(device))
            Z2 = model(X2.to(device))

            totalLoss += simclr_loss_func(Z1,Z2)/len(dataloader)
            
    testLoss.append(totalLoss.item())
    print("Test loss =",totalLoss.item() )

fracPrune = 0.99
totalPrune = 25
pruneNumber = sum([p.numel() for p in model.get_conv_param()])*fracPrune
pruneNumber = pruneNumber*(1/totalPrune)
print("pruneNumber = ",pruneNumber)
for t in range(1,totalPrune+1):
    logging.info("TEST")
    test(test_dataloader,model)
    logging.info(f"TRAIN epoch {t+1}")
    train(train_dataloader,model)
    
    saliency = []
    for p in model.get_conv_param():
        saliency.append( (p.detach().square()*optimizer.state[p]['exp_avg_sq']).view(-1))
    saliency = torch.cat(saliency)
    print(saliency.shape , int(pruneNumber*t))
    index = torch.topk(saliency,  int(pruneNumber*t),largest=False  ).indices
    count = 0
    mask = torch.ones_like(saliency)
    for i in index:
        mask[i] = 0
    print("number of elem to remov=",sum(mask==0))
    print(len(index))
    for index, (p,g) in enumerate( zip( model.get_conv_param(),gradMask)):
        cMask = mask[count:count+p.numel()].view_as(p)
        optimizer.state[p]['exp_avg'] = optimizer.state[p]['exp_avg']*cMask
        gradMask[index] = g*cMask
        p.data = p.data*cMask
        count += p.numel()

    print("trainable Param=",sum([g.sum() for g in gradMask ]))
    print("number of zero elem=",sum([ (p==0).sum() for p in model.get_conv_param()]) )
    for i in range(1):
        train(train_dataloader,model)
    print("number of zero elem=",sum([ (p==0).sum() for p in model.get_conv_param()]) )
    # logging.info("COPY")
    # modelSave = copy.deepcopy(model)
    # modelSave.linear_stack = architecture.ResMaxConvNeuralNetwork4().linear_stack.to(device)
    logging.info("SAVE")
    if (t+1)%1==0:
        torch.save(model,f"/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full_prune_ckpt_{t}")
        np.save("/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/SS-learning/testLoss",np.array( testLoss))
    # del modelSave
    

    