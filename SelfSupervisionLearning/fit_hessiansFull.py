import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/priors")
# sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning")
import copy

import hessian2
import torch
import os
from torch import nn
from torch.utils.data import DataLoader
from imageNetLoader import *
from constrastiveLoss import simclr_loss_func , simclr_indiv_loss_func

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")
args = sys.argv[1:]

logging.basicConfig(filename=f"/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/Grid_Output/logger_0",
                    filemode='a')
directories =  os.listdir("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/")
directories = [d for d in directories if d[0]=="n"]
directories = directories[::1]

# split_idx = [i for i in range(10)]
# testDataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",
#     split_idx,transform=True,animalList = directories,contrastive=True)

split_idx = [i for i in range(100+0,100+0 + 200 )]
trainDataset = OnlineCustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",
    split_idx,transform=True,animalList = directories,contrastive=True)

cpu_count = os.cpu_count()
batch_size = 256*2
num_workers= cpu_count
train_dataloader = DataLoader(
    trainDataset, batch_size=batch_size, shuffle=True,num_workers=num_workers)

myModel =  torch.load(f"/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full2_ckpt_19") 

def loss(data,model):
    X1,X2 = data
    Z1 = model(X1.to(device))
    Z2 = model(X2.to(device))
    # error= simclr_loss_func(Z1,Z2)#+ sum([p.square().sum() for p in model.parameters()])*1e-4
    error = simclr_indiv_loss_func(Z1,Z2).mean()
    # print("error=",error)
    return error



## fit hessian
for p in myModel.get_conv_param():
    p.requires_grad = True
for p in myModel.linear_stack.parameters():
    p.requires_grad = False

gradMask = torch.cat([(p != 0).view(-1) for p in myModel.get_conv_param()])
gradMask = torch.ones_like(gradMask)
# gradMask = torch.cat([gradMask , torch.ones(sum([p.numel() for p in myModel.linear_stack.parameters()]),device=device)])

parameters = []
for conv_stack in [1,2,3,4]:
    layer = myModel.__getattr__(f"conv_stack{conv_stack}")
    for p in layer.named_parameters(prefix=f"conv_stack{conv_stack}"):
        parameters.append(p)
# layer = myModel.__getattr__(f"conv_stack{conv_stack}")
# parameters = [p for p in layer.named_parameters(prefix=f"conv_stack{conv_stack}")]

D = sum([p[1].numel() for p in parameters ])
print("num param=",D)

hessianApprox = hessian2.SubHessianApproxFastPCA(parameters,device,rank=1000,buffer_size= 1000)
for i in range(10):
    hessian2.fit_hessianApprox_general(myModel,[p[1] for p  in parameters],train_dataloader,loss,hessianApprox,num_samples=1000)
    print(hessianApprox.diag.min())
    print(hessianApprox.diag.max())
    # hessianApprox.buffer = None
    hessianCopy = copy.deepcopy(hessianApprox)
    hessianCopy.buffer = None
    torch.save(hessianCopy,f"/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/SS-learning/Hessian/hessianFull")
    # torch.save(hessianCopy,f"/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/SS-learning/Conv1/conv1hess")