import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/")

import os
import torch
from tqdm import tqdm
from torch import nn
import fast_PCA
import logging
import numpy as np

def fit_hessianApprox_general(model,parameters,train_dataloader,loss,hessianApprox,prior=None,num_samples  = None):
    if( num_samples==None):
        num_samples = len(train_dataloader.dataset)
    for p in model.parameters():
        p.requires_grad = False
    for (p) in parameters:
        p.requires_grad = True
    # n= 0
    grad_mean = None
    grad_norm_mean = 0
    sample = 0
    optimizer = torch.optim.Adam(parameters,lr=5e-5,betas=[0.5,0.99])
    while sample < num_samples:
        for data in tqdm( train_dataloader):
            optimizer.zero_grad()
            lossValue = loss(data,model)
            lossValue.backward()
            hessianApprox.collect_grad(parameters)
            if prior != None:
                priorPen = prior(model)
                priorPen.backward()
            grad = torch.cat([p.grad.view(-1).detach() for p in parameters if p.requires_grad])
            optimizer.step()
            if grad_mean == None:
                grad_mean = grad
            else:
                grad_mean = (sample*grad_mean + grad)/(sample+1)
            
            grad_norm_mean = (grad_norm_mean*sample + grad.norm().square())/(sample+1)

            if sample == num_samples:
                break
            sample += 1
        print("grad mean norm =  " , grad_mean.norm(), " \n grad norm mean = ", grad_norm_mean.sqrt())

def fit_hessianApproxMask(model,parameters,train_dataloader,loss,hessianApprox,gradMask,prior=None,num_samples  = None):
    if( num_samples==None):
        num_samples = len(train_dataloader.dataset)
    for p in model.parameters():
        p.requires_grad = False
    for (p) in parameters:
        p.requires_grad = True
    # n= 0
    grad_mean = None
    grad_norm_mean = 0
    sample = 0
    optimizer = torch.optim.Adam(parameters,lr=5e-5,betas=[0.5,0.99])
    while sample < num_samples:
        for data in tqdm( train_dataloader):
            optimizer.zero_grad()
            lossValue = loss(data,model)
            lossValue.backward()
            for p,m in zip(parameters,gradMask):
                p.grad = p.grad*m
            hessianApprox.collect_grad(parameters)
            if prior != None:
                priorPen = prior(model)
                priorPen.backward()
            grad = torch.cat([p.grad.view(-1).detach() for p in parameters if p.requires_grad])
            optimizer.step()
            if grad_mean == None:
                grad_mean = grad
            else:
                grad_mean = (sample*grad_mean + grad)/(sample+1)
            
            grad_norm_mean = (grad_norm_mean*sample + grad.norm().square())/(sample+1)

            if sample == num_samples:
                break
            sample += 1
        print("grad mean norm =  " , grad_mean.norm(), " \n grad norm mean = ", grad_norm_mean.sqrt())

def fit_hessianApprox_general2(model,parameters,train_dataloader,likehood,prior,hessianApprox,num_samples  = None):
    # if(train_dataloader.batch_size != 1):
    #     logging.warning("train_dataloader.batch_size != 1")
    if( num_samples==None):
        num_samples = len(train_dataloader.dataset)
    for p in model.parameters():
        p.requires_grad = False
    for (p) in parameters:
        p.requires_grad = True
    grad_mean = None
    grad_norm_mean = 0
    sample = 0
    optimizer = torch.optim.Adam(parameters,lr=1e-4,betas=[0.5,0.9])

    while sample < num_samples:
        for data in tqdm( train_dataloader):
            
            lossValue = likehood(data,model)
            lossValue = lossValue/len(lossValue)
            # print("lossValue=",lossValue)
            idxs = np.random.choice(len(lossValue),size=  3,replace=False)#int( len(lossValue)*0.01))
            for i in ( idxs):
                optimizer.zero_grad()
                lossValue[i].backward(retain_graph=True)
                
                
                hessianApprox.collect_grad(parameters)
                
                grad = torch.cat([p.grad.view(-1).detach() for p in parameters if p.requires_grad])
                if grad_mean == None:
                    grad_mean = grad
                else:
                    grad_mean = (sample*grad_mean + grad)/(sample+1)
            
                grad_norm_mean = (grad_norm_mean*sample + grad.norm().square())/(sample+1)

                
                sample += 1
                if sample == num_samples:
                    break
                
            if sample == num_samples:
                    break
                
            # grads = compute_grads(lossValue)
            # print(grads.shape)
            # input()
            optimizer.zero_grad()
            lossValue = lossValue.sum()
            lossValue += prior(model)
            lossValue.backward()
            # grad = torch.cat([p.grad.view(-1).detach() for p in model.parameters() if p.requires_grad])
            optimizer.step()
            
        print("grad mean norm =  " , grad_mean.norm(), " \n grad norm mean = ", grad_norm_mean.sqrt())
