import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/")

import os
import torch
from tqdm import tqdm
from torch import nn
import logging
from sgld import *

class SWAG():
    def __init__(self,model) -> None:
        self.device = "cuda"
        self.mean = torch.zeros(sum([p.numel() for p in model.parameters()])).to(self.device)
        self.n = 0
        self.origine = torch.cat([p.detach().clone().view(-1) for p in model.parameters()])
        pass

    def collect_point(self,model):
        point = torch.cat([p.detach().view(-1) for p in model.parameters()])

        self.mean = (self.n*self.mean + point)/(self.n+1)
        self.n += 1

    def get_deviation(self):
        return (self.mean-self.origine).norm()

def fit_SWAG(train_dataloader,model,likehood,prior,lr=0.01,epochs=100,swag = None):

    device = "cuda"

    if swag == None:
        swag = SWAG(model)

    optimizer = SGLD(model.parameters(),lr=lr,temperature=5e-6)

    for epoch in range(epochs):
        for batch, (X,y) in enumerate(train_dataloader):
            X,y = X.to(device),y.to(device)
            pred = model(X)
            loss = likehood(pred,y) + prior(model)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if batch%10==0:
                swag.collect_point(model)
                print(f"loss = {loss.item()} , batch={batch}/{len(train_dataloader)}")

def castVec2Param(model,vec):
    count = 0
    param = []
    for p in model.parameters():
        param.append(vec[count:count+p.numel()].view(p.shape))
        count += p.numel()
    return param