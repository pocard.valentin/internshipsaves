import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/")

import os
import torch
from tqdm import tqdm
from torch import nn
import fast_PCA
import logging
import numpy as np

class SubHessian():
    def __init__(self,hessian,mean) -> None:
        self.W = hessian.W
        self.eigen = hessian.eigen
        self.diag = hessian.diag
        validW = self.W.norm(dim=1) != 0
        self.eigen = self.eigen[validW]
        self.W = self.W[validW]
        diagW = self.W.square().sum(dim=0).view(-1)
        self.newDiag = self.diag - diagW
        self.newDiag[self.newDiag<0] = torch.ones_like(self.newDiag[self.newDiag<0])*1e-12        
        # print(self.eigen)
        # print("min new diag=",self.diag.min())
        # print("max new diag=", self.diag.max())
        
        for i in range(len(self.W)):
            assert (self.W[i].norm()!=0)
            self.W[i] = self.W[i]/self.W[i].norm()
        self.mean = mean

    def error(self,parameters):
        theta = torch.cat([p.view(-1) for p in parameters])
        dTheta = (theta - self.mean).view(-1,1)
        projW   = self.W.mm( dTheta)

        residu = dTheta - self.W.T.mm(projW)
        erreur = (projW.square().view(-1).dot(self.eigen)).sum()  + (residu.square().view(-1).dot(self.newDiag)).sum()
        # erreur = (dTheta.square().view(-1).dot(self.diag)).sum()
        return erreur*0.5
    

class Hessian():
    def __init__(self,hessianList,paramList) -> None:
        id =0
        self.SubHessianList = []

        for (hess,params) in zip(hessianList,paramList):
            paramLabel = []
            mean = None
            for label,param in params:
                paramLabel.append(label)
                if mean == None:
                    mean = param.detach().clone().view(-1)
                else:
                    mean = torch.cat( [ mean , param.detach().clone().view(-1)])
            subHess = SubHessian(hess,mean)
            self.SubHessianList.append( (subHess,paramLabel)  )
    def error(self,model):
        totalError = 0
        First = True
        for (subHessian,paramLabel) in self.SubHessianList:
            

            parameters = []
            for label in paramLabel:
                parameters.append(model.get_parameter(label))
            if First:
                First = False
            totalError += subHessian.error(parameters)

        return totalError



class SubHessianSubNetwork():
    def __init__(self,hessian,mean) -> None:
        self.W = hessian.W
        self.eigen = hessian.eigen
        self.penRes = max( hessian.diag.max()*100,1e4)
        # print(self.eigen)
        # print("min new diag=",self.diag.min())
        # print("max new diag=", self.diag.max())
        validW = self.W.norm(dim=1) != 0
        self.eigen = self.eigen[validW]
        self.W = self.W[validW]
        for i in range(len(self.W)):
            assert (self.W[i].norm()!=0)
            self.W[i] = self.W[i]/self.W[i].norm()
        self.mean = mean

    def error(self,parameters,*args):
        args=args[0]
        theta = torch.cat([p.view(-1) for p in parameters])
        dTheta = (theta - self.mean).view(-1,1)
        projW   = self.W.mm( dTheta)

        residu = dTheta - self.W.T.mm(projW)
        if args == None:
            erreur = (projW.square().view(-1).dot(self.eigen)).sum()  + (residu.square().view(-1)*self.penRes).sum()
        else:
            erreur = (projW.square().view(-1).dot(self.eigen)).sum()  + (residu.square().view(-1)).sum()*args
        # erreur = (dTheta.square().view(-1).dot(self.diag)).sum()
        return erreur*0.5

class HessianSubNetwork():
    def __init__(self,hessianList,paramList) -> None:
        id =0
        self.SubHessianList = []

        for (hess,params) in zip(hessianList,paramList):
            paramLabel = []
            mean = None
            for label,param in params:
                paramLabel.append(label)
                if mean == None:
                    mean = param.detach().clone().view(-1)
                else:
                    mean = torch.cat( [ mean , param.detach().clone().view(-1)])
            subHess = SubHessianSubNetwork(hess,mean)
            self.SubHessianList.append( (subHess,paramLabel)  )
    def error(self,model,*args):
        if len(args) != 0:
            args = args[0]
        else:
            args=None
        totalError = 0
        First = True
        for (subHessian,paramLabel) in self.SubHessianList:
            

            parameters = []
            for label in paramLabel:
                parameters.append(model.get_parameter(label))
            if First:
                First = False
            totalError += subHessian.error(parameters,args)

        return totalError

class SubHessianApproxFastPCA(nn.Module):
    def __init__(self,parameters, device, rank = 10,buffer_size=100 , dtype = torch.float32):
        self.n = 0
        self.dtype = dtype
        self.device = device
        self.size =0
        self.paramLabel = []
        for (label,param) in parameters:
             self.size += param.numel()
             self.paramLabel.append(label)
        size = self.size
        self.diag = torch.zeros(size,device=device,dtype=dtype)
        if rank > self.size:
            rank = self.size
        self.W    = torch.zeros((rank,size),device=device,dtype=dtype)
        self.buffer = torch.zeros((buffer_size,size),device=device,dtype=dtype)
        self.eigen = torch.zeros((rank),device=device,dtype=dtype)
        self.rank = rank
        self.buffer_size = buffer_size
        self.wCount = 0

    def mv(self,v):
                v = v.type(self.dtype)
                # print("vShape =",v.shape)
                xNew = self.buffer.matmul( v)
                # print("xNew shape =",xNew.shape)
                # print("buffer shape=",self.buffer.shape)
                xNew = torch.einsum("lk,ki->li",self.buffer.T,xNew)/self.buffer_size
                xOld = self.W.matmul( v)
                # print("xOld shape = ",xOld.shape)
                # print("W shape =",self.W.shape)
                xOld = torch.einsum("lk,ki->li",self.W.T,xOld)

                return (self.wCount*xOld+xNew)/(self.wCount+1)
    
    def collect_grad(self, parameters):
        grad = torch.cat([p.grad.view(-1).detach() for p in parameters if p.requires_grad])
        grad = grad.type(self.dtype)
        
        self.diag = (self.n*self.diag + grad.square())/(self.n+1)
        self.n = self.n+1
        
        indice = self.n%self.buffer_size
        grad = grad
        self.buffer[indice] = grad
        if indice ==0:
            eigenvalues, eigenvectors = fast_PCA.eigsh_func(self.mv,self.dtype,self.device,len(grad),k=self.rank,n_iter=2)
            # eigenvalues, eigenvectors = fast_PCA.eigsh_func(self.mv,torch.float32,self.device,len(grad),k=self.rank,n_iter=2)
            for index in range(len(eigenvalues)):
                
                self.W[index] = ( max(eigenvalues[-index-1],0)).sqrt()*eigenvectors[:,-index-1] 
                # print((self.W[index]==0).sum())
                self.eigen[index] = max(eigenvalues[-index-1],0)
            self.wCount+=1

