import numpy as np
import os
elem = [0 , 2 , 4,6,8,10,12]
pen = [3]
offset = [0,1,2,3,4,5]
count = 0
for numElem in elem:
    for penElem in pen:
        for offsetElem in offset:
            path = f"/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment2/ResultMin/{numElem}/{penElem}.0_penal_{50}_samples_run{offsetElem}"
            # print(path)
            skip = False
            try:
                data = np.load(path+".npy")
                skip =  ( data.shape[0] == 100)
                # print("skip=",skip)
            except:
                pass
            # print(skip)
            if skip: continue
            commande = f"qsub job8       {penElem} {numElem} {offsetElem} "
            print(commande)
            # input()
            os.system(commande)
            count += 1

print("total jobs sent =",count)