import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/priors/")

import numpy as np

import torch

from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
import os
import architecture
from imageNetLoader import *
import SWAG
# from evidence import compute_LogEvidence

args = sys.argv[1:]
logPen = float(args[0])
numElem = int(args[1])
run = int(args[2])

hessianApprox = torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Hessian/EWC")

diag = hessianApprox.SubHessianList[0][0].diag
print(len(diag))
indices = torch.topk(diag, min(len(diag),  numElem*10000) ).indices

gradMask = torch.zeros_like(diag) #hessianApprox.diag >= minIndex
gradMask[indices] = 1

# index =  prec <= float( args[1])
# minIndex = hessianApprox.diag.sort()[0][index].max()
# gradMask = hessianApprox.diag <= minIndex

print("precision=",args[1] , "param garde=",sum(gradMask))

directories =  os.listdir("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/")
directories = [d for d in directories if d[0]=="n"]
directories = directories[::4][:64]
# directories = np.load("/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/Saves/RepresentationList.npy")

print(len(directories))
split_idx = [i for i in range(100)]
testDataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",split_idx,transform=False,animalList = directories)
split_idx = [i for i in range(100,100+5 )]

# split_idx = [i for i in range(100)]
# testADataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",split_idx,transform=False,animalList = directories)


device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")
myModel =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full2_ckpt_19")  # architecture.ResMaxConvNeuralNetwork().to(device)
myModel.linear_stack = architecture.ResMaxConvNeuralNetwork5().to(device).linear_stack

startingPoint = [p.clone().detach() for p in myModel.parameters()]
startingConv = [p.clone().detach() for p in myModel.get_conv_param()]
startingLinear = [torch.zeros_like(p) for p in myModel.linear_stack.parameters()]
print(sum([p.norm() for p in startingLinear]))
# swag = torch.load("/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/Saves/swag")
# startingPoint = SWAG.castVec2Param(myModel,swag.mean)

cpu_count = 1# os.cpu_count()
logging.info(f"cpu count = {cpu_count}")
D = sum([p.numel() for p in myModel.parameters()])
print(f"Number of parameters : {D}")

logging.info(f"regularization strength = 10**{logPen}")


batch_size = 64
num_workers=cpu_count
test_dataloader = DataLoader(
    testDataset, batch_size=batch_size*4, shuffle=False,num_workers=num_workers,pin_memory=True)
# testA_dataloader = DataLoader(
#     testADataset, batch_size=batch_size*4, shuffle=False,num_workers=num_workers,pin_memory=True)

def computeNorm(param1,param2):
    sqNorm = 0
    for p1,p2 in zip(param1,param2):
        sqNorm += (p1-p2).square().sum()
    return sqNorm
def penalty(model,lambdaPen):
    convPenal = computeNorm(model.get_conv_param(),startingConv)*lambdaPen
    linPenal  = computeNorm(model.linear_stack.parameters(),startingLinear)*1.5e1
    return convPenal+linPenal



def train(dataloader, model, loss_fn, optimizer):
    
    model.train()
    for batch, (X, y) in enumerate(dataloader):
        X, y = X.to(device), y.to(device)
        pred = model(X)
        likehood = loss_fn(pred, y)
        logPrior = penalty(model,10**logPen)/size
        loss = likehood + logPrior
        optimizer.zero_grad()
        loss.backward()
        count = 0
        for p in model.get_conv_param():
            if not p.requires_grad:
                continue
            p.grad = p.grad*gradMask[count:count+p.numel()].view_as(p)
            count += p.numel()

        optimizer.step()
        if batch % 50 == 0:
            loss, current = likehood.item(), batch * len(X)
            print(
                f"loss: {loss:>7f} logPrior : {logPrior.item():>7f} [{current:>5d}/{size:>5d}]")


def test(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    model.eval()
    test_loss, correct , correct5 = 0, 0,0
    with torch.no_grad():
        for X, y in dataloader:
            X, y = X.to(device), y.to(device)
            pred = model(X)
            test_loss += loss_fn(pred, y).item()
            correct += (pred.argmax(1) == y).type(torch.float).sum().item()
            top5 = torch.topk(pred,5,dim=1).indices
            correct5 += (top5.T==y).type(torch.float).sum().item()

            
    test_loss /= num_batches
    correct /= size
    correct5 /= size
    logging.info(
        f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Accuracy5: {(100*correct5):>0.1f}%  ,  Avg loss: {test_loss:>8f} \n")
    return correct, test_loss , correct5

epochs = 100
loss_fn = nn.CrossEntropyLoss()

# param = myModel.get_conv_param()
# for p in param:
#     p.requires_grad = False
labels = np.load("/remote/idiap.svm/temp.speech01/vpocard/ImageNetTF/More_classes/labels2.npy")

D = sum([p.numel() for p in myModel.parameters() if p.requires_grad]) - sum(gradMask==False)
print(f"Number of trainable parameters : {D}")
nb_runs = 1
evidence = [ ]
# modelA =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_2_ckpt_65")
directories =  os.listdir("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/")
directories = [d for d in directories if d[0]=="n"]

directories = [directories[index] for index in labels[run]]

split_idx = [i for i in range(100,100+50 )]
path = f"/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment2/ResultMax/{numElem}/{logPen}_penal_{len(split_idx)}_samples_run{run}"
skip = False
try:
    data = np.load(path+".npy")
    skip = ( data.shape[0] == epochs)
    print("skip=",skip)
except:
    pass
if not skip:
    trainDataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",split_idx,transform=False,animalList = directories)
    split_idx = [i for i in range(50)]
    testDataset = CustomImageDataset("/idiap/temp/vpocard/imagenet/ILSVRC2012/ILSVRC2012_images_train/",split_idx,transform=False,animalList = directories)
    batch_size = 64
    num_workers=cpu_count
    train_dataloader = DataLoader(
        trainDataset, batch_size=batch_size, shuffle=True,num_workers=num_workers)
    test_dataloader = DataLoader(
        testDataset, batch_size=batch_size*4, shuffle=False,num_workers=num_workers)
    size = len(train_dataloader.dataset)

    myModel =  torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Saves/model_full2_ckpt_19")
    myModel.linear_stack = architecture.ResMaxConvNeuralNetwork5().to(device).linear_stack

    optimizer = torch.optim.Adam(myModel.parameters(), lr=5e-4)
    # optimizer = torch.optim.Adam(myModel.linear_stack.parameters(), lr=1e-3)

    # scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min',patience=3,factor=0.5)

    scheduler = torch.optim.lr_scheduler.StepLR(optimizer,3,0.95)
    datas = []
    for t in range(epochs):

        # train_dataloader.num_workers = 0*(t==0) + cpu_count*(t!=0) #Allow to cache data during the first epoch
        # test_dataloader.num_workers = 0*(t==0) + cpu_count*(t!=0) #Allow to cache data during the first epoch
        print(f"Epoch {t+1}\n-------------------------------")

        if t%2==0: data= list(test(test_dataloader, myModel, loss_fn))
        # scheduler.step(data[1])#test_loss
        # data2 = test(testA_dataloader, myModel, loss_fn)
        # for d in data2:
        #     data.append(d)
        print('Epoch-{0} lr: {1}'.format(t+1, optimizer.param_groups[0]['lr']))

        train(train_dataloader, myModel, loss_fn, optimizer)
        scheduler.step()
        datas.append(data)
        np.save(path,datas)
    torch.save(myModel,f"/idiap/temp/vpocard/ImageNetTF/More_classes/Experiment2/ResultMax/{numElem}/{logPen}_penal_model_run_{run}")
    del trainDataset ,  testDataset , train_dataloader,test_dataloader