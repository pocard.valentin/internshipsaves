import os
from torchvision.io import read_image
import torch
from torch.utils.data import Dataset
from torchvision.transforms import *
from tqdm import tqdm
from torch import nn
import numpy as np
import logging
import torch.multiprocessing as mp


class ResMaxConvNeuralNetwork5(nn.Module):
    def __init__(self):
        super().__init__()
        dropoutProb = 1
        self.conv_stack1 = nn.Sequential(
            nn.Conv2d(3, 4, kernel_size=(7, 7), stride=(2,2),padding=(3,3)),
            nn.ReLU(),
            nn.Conv2d(4, 8, kernel_size=(7, 7), stride=(2,2),padding=(3,3)),
            nn.ReLU(),
            nn.BatchNorm2d(8),)
            
        self.conv_stack2 = nn.Sequential(
            nn.Conv2d(8, 12, kernel_size=(7, 7), stride=(2,2),padding=(3,3)),
            nn.ReLU(),
            nn.Conv2d(12, 16, kernel_size=(7, 7), stride=(2,2),padding=(3,3)),
            nn.ReLU(),
            nn.BatchNorm2d(16),)
        self.conv_stack3 = nn.Sequential(

            nn.Conv2d(16, 24, kernel_size=(5, 5), stride=(1,1)),
            nn.ReLU(),
            nn.Conv2d(24, 32, kernel_size=(5, 5), stride=(1,1)),
            nn.ReLU(),
            nn.BatchNorm2d(32),)
        self.conv_stack4 = nn.Sequential(
            nn.Conv2d(32, 48, kernel_size=(4, 4), stride=(1,1)),
            nn.ReLU(),
            nn.Conv2d(48, 64, kernel_size=(4, 4), stride=(1,1)),
            nn.ReLU()
            ,nn.BatchNorm2d(64),)
        self.avg =  nn.Sequential(   nn.AdaptiveMaxPool2d((1,1)),
            nn.Flatten()
        )



        

        self.linear_stack = nn.Sequential(
            nn.Dropout(p=0.05),
            nn.Linear(8+16+32+64, 128),
            nn.ReLU(),
            nn.Dropout(p=0.05),
            nn.Linear(128, 512))

    def get_conv_param(self):
        param = []
        for i in range(4):
            layer = self.__getattr__(f"conv_stack{i+1}")
            for p in layer.parameters():
                param.append(p)
        return param
    def get_conv_param2(self):
        param = []
        for i in range(4):
            layer = self.__getattr__(f"conv_stack{i+1}")
            for name, p in layer.named_parameters():
                param.append(p)
                print(name)
        return param
    def forward(self, x):
        n=4
        conv_stacks = [f"conv_stack{i+1}" for i in range(n)]
        feature = [x]
        for i in range(n):
            layer = self.__getattr__(f"conv_stack{i+1}")
            newFeature = layer(feature[-1])
            feature.append(newFeature)

        feature.pop(0)
        logits = torch.cat([self.avg(f).T for f in feature]).T
        
        logits = self.linear_stack(logits)
        
        return logits




