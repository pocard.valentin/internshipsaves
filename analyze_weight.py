import matplotlib.pyplot as plt
import matplotlib
import sys
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/")
sys.path.append("/idiap/temp/vpocard/ImageNetTF/More_classes/priors/")

import numpy as np

import torch

from torch import nn
run = 0
hessianApprox = torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Hessian/EWC","cpu")
hessianApprox2 = torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/SS-learning/Hessian/hessianCompiled2","cpu")
W = hessianApprox2.SubHessianList[0][0].W
# eigen = hessianApprox2.SubHessianList[0][0].eigen
# W = W*eigen[...,None].sqrt().cpu()

print(W)
diag2 = W.square().sum(dim=0)#hessianApprox.SubHessianList[0][0].diag# 
L2SP = True
if L2SP : model = torch.load(f"/idiap/temp/vpocard/ImageNetTF/More_classes/ResultArch5/model_saves/L2SP/3.0_penal_model_run_{run}","cpu")
else    : model = torch.load("/idiap/temp/vpocard/ImageNetTF/More_classes/ResultArch5/model_saves/EWC/8.0_penal_model_run_0","cpu")
# model = torch.load(f"/idiap/temp/vpocard/ImageNetTF/More_classes/ResultArch5/model_saves/HESS/4.0_penal_model_run_{run}","cpu")

newTheta = nn.utils.convert_parameters.parameters_to_vector(model.get_conv_param()).detach()
center = hessianApprox2.SubHessianList[0][0].mean

# dTheta = (newTheta - center).view(-1,1)
# projW   = W.mm( dTheta)
# residu = dTheta - W.T.mm(projW)

# plt.scatter(range(len(projW)),projW.abs())
# plt.show()
# print(projW.norm(),residu.norm())
diag = hessianApprox.SubHessianList[0][0].diag
print(diag.norm(),diag2.norm(),(diag-diag2).norm()/diag2.norm())
print("len diag=",len(diag))
numBin = 10
binSize = int( np.floor( len(diag)/numBin))
start = len(diag) -binSize*numBin
topk = torch.topk(diag,k=  binSize*numBin)
indices = []
for bin in range(numBin):
    indices.append(topk.indices[bin*binSize:(bin+1)*binSize])

weightChange = []
means = []
stds = []
labels = []
for indice in indices:
    pStart = center[indice]
    pChange = newTheta[indice]
    relativeChange = ((pStart-pChange)).square()#/pStart
    label = diag[indice].mean()*50*500
    label = label.log().item()
    label = round(label,3)
    labels.append(label)
    relativeChange = ((pStart-pChange)/pStart).abs()*100#/pStart
    q = torch.tensor([0.1,0.9]).cpu()
    lowerB,higherB = relativeChange.quantile(q)
    extractIndice = torch.logical_and(relativeChange>lowerB , relativeChange < higherB) 
    relativeChange = relativeChange[extractIndice]
    # means.append(relativeChange.mean().cpu().numpy())
    means.append(relativeChange.median().cpu().numpy())
    stds.append(relativeChange.std().cpu().numpy())
    weightChange.append( relativeChange.cpu().numpy()  )
# weightChange = np.array(weightChange)

# plt.errorbar(range(numBin),means,stds)
# plt.scatter(range(numBin),means)
# plt.errorbar(range(numBin),means, stds, fmt="o")

# plt.show()
font = {'family' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

plt.boxplot(weightChange,sym="")
plt.ylabel("relative change of the weight (%)",fontsize="20")
plt.xlabel("Parameters precision , (log scale) ",fontsize="20")
plt.xticks(range(1,len(weightChange)+1),labels)

if L2SP : title = "relative weight change on the downstream task with L2SP prior"
else    : title = "relative weight change on the downstream task with EWC prior"
# plt.title(title)
plt.show()
print(start)
print(diag.min())
print(diag.max())


